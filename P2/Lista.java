import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <p> Clase concreta para modelar la estructura de datos Lista</p>
 * <p>Esta clase implementa una Lista genérica, es decir que es homogénea pero
 * puede tener elementos de cualquier tipo.
 * @author Ruiz Castillo Ricardo Yoaly <gaanzz11@ciencias.unam.mx>
 * @author Luis Manuel Martinez Damaso <luismanuel@ciencias.unam.mx>
 * @version 1.0
 * @param <T>
 */
public class Lista<T> implements Listable<T>{

    /* Clase interna para construir la estructura */
    protected class Nodo{
        /* Referencias a los nodos anterior y siguiente */
        public Nodo anterior, siguiente;
        /* El elemento que almacena un nodo */
        public T elemento;

        /* Unico constructor de la clase */
        public Nodo(T elemento){
        //Aquí va tu código.
            this.elemento = elemento;
        }
        public boolean equals(Nodo n){
            //Aquí va tu código.
            if(n.elemento == this.elemento){
                return true;
            }else{
                return false;
            }

        }
    }

    
    private class IteradorLista implements Iterator<T>{
        /* La lista a recorrer*/
        /* Elementos del centinela que recorre la lista*/

        private Lista<T>.Nodo anterior;
        private Lista<T>.Nodo siguiente;

        public IteradorLista(){
            //Aquí va tu código.
            anterior = null;
            siguiente = cabeza;
        }

        @Override
        public boolean hasNext() {
            //Aquí va tu código.
            if(siguiente == null){
                return false;
            }
            else{
                return true;
            }
        }

        @Override
        public T next() {
            //Aquí va tu código.
            if ( siguiente == null){
                throw new NoSuchElementException();
            }else{
                anterior = siguiente;
                siguiente = siguiente.siguiente;
                return anterior.elemento;
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    
    /* Atributos de la lista */
    protected Nodo cabeza, cola;
    protected int longitud;

    /**
     *  Constructor por omisión de la clase, no recibe parámetros.
     *  Crea una nueva lista con longitud 0.
     **/
    public Lista(){
        //Aquí va tu código.
        cabeza = null;
        cola = null;
        longitud = 0;
         
    }

    /**
     *  Constructor copia de la clase. Recibe una lista con elementos.
     *  Crea una nueva lista exactamente igual a la que recibimos como parámetro.
     * @param l
     **/
    public Lista(Lista<T> l){
        //Aquí va tu código.
        cabeza = l.cabeza;
        cola = l.cola;
        longitud = l.longitud;
    }

    /**
     *  Constructor de la clase que recibe parámetros.
     *  Crea una nueva lista con los elementos de la estructura iterable que recibe como parámetro.
     * @param iterable
     **/
    public Lista(Iterable<T> iterable){
        //Aquí va tu código.
        for (T elem : iterable) {
            agregar(elem);
        }
    
    }
    
    /**
     * Método que nos dice si las lista está vacía.
     * @return <code>true</code> si el conjunto está vacío, <code>false</code>
     * en otro caso.
     */
    @Override
    public boolean esVacia(){
        //Aquí va tu código.
        if(getTamanio() == 0){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Método para eliminar todos los elementos de una lista
     */
    @Override
    public void vaciar(){
        //Aquí va tu código.
        longitud = 0;
        cabeza = null;
        cola = null;
    }
    
    /**
     * Método para obtener el tamaño de la lista
     * @return tamanio Número de elementos de la lista.
     **/
    @Override
    public int getTamanio(){
        //Aquí va tu código.
        return longitud;
    }
    
    /**
     * Método para agregar un elemento a la lista.
     * @param elemento Objeto que se agregará a la lista.
     */
    @Override
    public void agregar(T elemento){
        //Aquí va tu código.
        if(elemento == null){
            throw new IllegalArgumentException();
        }else{
            if(cabeza == null){
                cabeza = new Nodo(elemento);
                cola = cabeza;
            }else{
                cabeza.anterior = new Nodo(elemento);
                cabeza.anterior.siguiente = cabeza;
                cabeza = cabeza.anterior;
            }
        }
        longitud = longitud + 1;

    }
    
    /**
     * Método para agregar al final un elemento a la lista.
     * @param elemento Objeto que se agregará al inicio de la lista.
     */
    public void agregarAlFinal(T elemento) throws IllegalArgumentException {
        //Aquí va tu código.
        if(elemento == null){
            throw new IllegalArgumentException();
        }else{
            if(cabeza == null){
                cabeza = new Nodo(elemento);
                cola = cabeza;
            }else{
                cola.siguiente = new Nodo(elemento);
                cola.siguiente.anterior = cola;
                cola = cola.siguiente;
            }
        }
        longitud = longitud + 1;


    }

    /**
     * Método para obtener el primer elemento.
     */
    @Override
    public T getPrimero() throws NoSuchElementException {
        //Aquí va tu código.
        if(getTamanio() < 1){
            throw new NoSuchElementException();
        }else{
            return cabeza.elemento;
        }
    }

    /**
     * Método para obtener el último elemento.
     */
    public T getUltimo() throws NoSuchElementException {
        //Aquí va tu código.
        if(getTamanio() < 1){
            throw new NoSuchElementException();
        }else{
            return cola.elemento;
        }
    }
    
    /**
     * Método para verificar si un elemento pertenece a la lista.
     * @param elemento Objeto que se va a buscar en la lista.
     * @return <code>true</code> si el elemento esta en el lista y false en otro caso.
     */
    
    @Override
    public boolean contiene(T elemento) throws NoSuchElementException {
        //Aquí va tu código.
        if(indiceDe(elemento) > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Método para eliminar un elemento de la lista.
     * @param elemento Objeto que se eliminara de la lista.
     * todo
     */
    @Override
    public void eliminar(T elemento) throws NoSuchElementException {
        //Aquí va tu código.
        Nodo n = cabeza;
        while( n != null){
            if(n.elemento.equals(elemento)){
                if(n.siguiente == null){
                    eliminarUltimo();
                }else{
                    if(n.anterior == null){
                        eliminarPrimero();
                    }else{
                        n.siguiente.anterior = n.anterior;
                        n.anterior.siguiente = n.siguiente;
                        longitud = longitud - 1;
                    }
                }
                break;
            }
            n = n.siguiente;
        }
    }

    /**
     * Método para eliminar el primer elemento de la lista.
     */
    @Override
    public void eliminarPrimero() throws NoSuchElementException {
        //Aquí va tu código.
        if(getTamanio() < 1) throw new NoSuchElementException();

        if(getTamanio() == 1){
            cabeza = cola;
            cola = null;
        }else{
            cabeza = cabeza.siguiente;
            cabeza.anterior = null;
        }
        longitud = longitud -1;
    }

    /**
     * Método para eliminar el primer elemento de la lista.
     */
    public void eliminarUltimo() throws NoSuchElementException {
        //Aquí va tu código.
        if(cola == null){
            throw new NoSuchElementException();
        }else{
            if(getTamanio() == 1){
                cola = cabeza;
                cabeza = null;
            }else{
                cola = cola.anterior;
                cola.siguiente = null;
            }
            longitud = longitud - 1;
        }
    }


    /**
     * Método que devuelve la posición en la lista que tiene la primera
     * aparición del <code> elemento</code>.
     * @param elemento El elemnto del cuál queremos saber su posición.
     * @return i la posición del elemento en la lista, -1, si no se encuentra en ésta.
     */

    public int indiceDeAux(Nodo n, T elemento, int index){
        if(n == null){
            return -1;
        }else{
            if(n.elemento.equals(elemento)){
                return index;
            }
        }

        return indiceDeAux(n.siguiente, elemento, index + 1);
    }

    @Override
    public int indiceDe(T elemento) throws NoSuchElementException {
        //Aquí va tu código.
        return indiceDeAux(cabeza, elemento, 0);
    }

    /**
     * Método que nos devuelve el elemento que esta en la posición i
     * @param i La posición cuyo elemento deseamos conocer.
     * @return <code> elemento </code> El elemento que contiene la lista,
     * <code>null</code> si no se encuentra
     * @throws IndexOutOfBoundsException Si el índice es < 0 o >longitud()
     */

    public T getElementoAux(Nodo n,int index, int contador){
            
        if(contador == index){
            return n.elemento;
        }else{
            return getElementoAux(n.siguiente, index, contador + 1);
        }
    }
    @Override
    public T getElemento(int i) throws IndexOutOfBoundsException{
        //Aquí va tu código.
        if(i < 0 || i > getTamanio()){
            throw new IndexOutOfBoundsException();
        }else{
            return getElementoAux(cabeza, i, 0);
        }
    }

    /**
     * Método auxiliar que devuelve una copia de la lista, pero en orden inverso
     * @return Una copia con la lista l revés.
     */
    public Lista<T> reversa(Lista<T> l, Nodo n){
        if(n == null){
            return l;
        }else{
            l.agregar(n.elemento);
            return reversa(l, n.siguiente);
        }
    }
    /**
     * Método que devuelve una copia de la lista, pero en orden inverso
     * @return Una copia con la lista l revés.
     */
    @Override
    public Lista<T> reversa(){
        //Aquí va tu código.
        Lista<T> reverse = new Lista<>();
        
        return reversa(reverse, cabeza);
    }


    /**
     * Método que devuelve una copia exacta de la lista
     * @return la copia de la lista.
     */

    @Override
    public Lista<T> copia(){
        //Aquí va tu código.
        Lista<T> tmp = new Lista<T>();

        tmp.longitud = 0;

        Nodo aux = this.cabeza;
        while(aux != null){
            tmp.agregarAlFinal(aux.elemento);
            aux = aux.siguiente;
        }
        return tmp;
    }

    /**
     * Método que nos dice si una lista es igual que otra.
     * @param o objeto a comparar con la lista.
     * @return <code>true</code> si son iguales, <code>false</code> en otro caso.
     */
    @Override
    public boolean equals(Object o){
        //Aquí va tu código.
        if(o instanceof Lista){
            @SuppressWarnings("unchecked") Lista<T> lista = (Lista<T>) o;
            if(getTamanio() != lista.getTamanio()){
                return false;
            }else{
                Nodo aux = cabeza;
                Nodo tmp = lista.cabeza;
                while(aux != null && tmp != null){
                    if(aux.elemento.equals(tmp.elemento)){
                        aux = aux.siguiente;
                        tmp = tmp.siguiente;
                    }else{
                        return false;
                    }
                }
                return true;
            }
            
        }else{
            return false;
        }
    }

    /**
     * Método que devuelve un iterador sobre la lista
     * @return java.util.Iterador -- iterador sobre la lista
     */
    @Override
    public java.util.Iterator<T> iterator(){
        return new IteradorLista();
    }


    @Override
    public String toString() {
        if (esVacia()) {
            return "[]";
        }
        Nodo nodo = cabeza;
        String cad = "[" + nodo.elemento;
        while (nodo.siguiente != null) {
            nodo = nodo.siguiente;
            cad += ", " + nodo.elemento;
        }
        return cad + "]";
    }
      
    
    /* Método auxiliar para obtener una referencia a un nodo con un elemento
    específico. Si no existe tal nodo, devuelve <code> null </code> */



    private Nodo getNodoAux(Nodo n, T elem){
        if(n.elemento == elem){
            return n;
        }else{
            if(n.siguiente == null){
                return null;
            }else{
                return getNodoAux(n.siguiente, elem);
            }
        }
    }

    private Nodo getNodo(T elem) throws NoSuchElementException {
        //Aquí va tu código.
        
        return getNodoAux(cabeza, elem);
        
    }

}
