public class PruebaPractica{

	public static void main(String[] args) {
		Pila<String> p = new Pila<>();
		Cola<String> c = new Cola<>();

		p.push("h");
		p.push("o");
		p.push("l");
		p.push("a");
		p.pop();
		p.push("e");
		String listaS = p.toString();
		System.out.println(listaS);
		System.out.println(p.top());


		c.queue("h");
		c.queue("o");
		c.queue("l");
		c.queue("a");
		c.dequeue();
		c.queue("e");

		String cola = c.toString();
		System.out.println(cola);
		System.out.println(c.peek());
	}
}