/**
 * <p>
 * Clase para árboles AVL.</p>
 *
 * <p>
 * Un árbol AVL cumple que para cada uno de sus nodos, la diferencia entre la
 * áltura de sus subárboles izquierdo y derecho está entre -1 y 1.</p>
 *
 * @param <T>
 */
public class ArbolAVL<T extends Comparable<T>> extends ArbolBinarioBusqueda<T> {

    /**
     * Clase interna protegida para nodos de árboles AVL. La única diferencia
     * con los nodos de árbol binario, es que tienen una variable de clase para
     * la altura del nodo.
     */
    protected class NodoAVL extends ArbolBinario.Nodo {

        /**
         * La altura del nodo.
         */
        protected int altura;

        /**
         * Constructor único que recibe un elemento.
         *
         * @param elemento el elemento del nodo.
         */
        public NodoAVL(T elemento) {
            // Aquí va su código.
            super(elemento);
            altura = 0;
        }
	
	/**
	 * Recomendamos usar este método auxiliar para que en el método público
	 * hagas el cast del objeto o, a NodoAVL y dejar el trabajo a este método.
  	 * Si no quieres usarlo, siéntete libre de eliminar esta firma.
	 */
        private boolean equals(NodoAVL v, NodoAVL v2) {
	    //Aquí va tu código.
        if (v == null && v2 == null) {
            return true;
        }
        if((v == null && v2 != null) || (v != null && v2 == null) || !v.elemento.equals(v2.elemento)){
          return false;
        }
        if(v.altura != v2.altura){
            return false;
        }
        NodoAVL vD = nodoAVL(v.derecho);
        NodoAVL vI = nodoAVL(v.izquierdo);
        NodoAVL v2D = nodoAVL(v2.derecho);
        NodoAVL v2I = nodoAVL(v2.izquierdo);

        return equals(vI, v2I) && equals(vD, v2D);
        }

        /**
         * Compara el nodo con otro objeto. La comparación es
         * <em>recursiva</em>.
         *
         * @param o el objeto con el cual se comparará el nodo.
         * @return <code>true</code> si el objeto es instancia de la clase
         * {@link NodoAVL}, su elemento es igual al elemento de éste nodo, los
         * descendientes de ambos son recursivamente iguales, y las alturas son
         * iguales; <code>false</code> en otro caso.
         */
        @Override
        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }
            if (getClass() != o.getClass()) {
                return false;
            }
            @SuppressWarnings("unchecked")
            NodoAVL nodo = (NodoAVL) o;
            return equals(this, nodo);
        }

        @Override
        public String toString() {
            String s = super.toString();
            return s += " alt=" + altura;
        }
    }

    public ArbolAVL() {

    }

    public ArbolAVL(Coleccionable<T> coleccion) {
        super(coleccion);
    }

    private void actualizaAltura(NodoAVL v) {
        // Aquí va su código.
        NodoAVL aux = nodoAVL(v);
        NodoAVL vI = nodoAVL(aux.izquierdo);
        NodoAVL vD = nodoAVL(aux.derecho);
        aux.altura = 1 + Math.max(getAltura(vI), getAltura(vD));
    }

    private int balance(NodoAVL nodo){
        NodoAVL aux = (NodoAVL) nodoAVL(nodo);
        NodoAVL vI = nodoAVL(aux.izquierdo);
        NodoAVL vD = nodoAVL(aux.derecho);
        return getAltura(vI) - getAltura(vD);
    }

    private void rebalancea(NodoAVL nodo) {

        if (nodo != null) {
            NodoAVL izq = nodoAVL(nodo.izquierdo), der = nodoAVL(nodo.derecho);
            int balance = getAltura(izq) - getAltura(der);
            if (balance == -2) {
                if (getAltura(nodoAVL(der.izquierdo)) - getAltura(nodoAVL(der.derecho)) == 1) {
                    rotacionDerecha(der);
                    actualizaAltura(der);
                }
                rotacionIzquierda(nodo);
            }
            if (balance == 2) {
                if (getAltura(nodoAVL(izq.izquierdo)) - getAltura(nodoAVL(izq.derecho)) == -1) {
                    rotacionIzquierda(izq);
                    actualizaAltura(izq);
                }
                rotacionDerecha(nodo);
            }
            actualizaAltura(nodo);
            rebalancea(nodoAVL(nodo.padre));
        }
    }

    /**
     * Agrega un nuevo elemento al árbol. El método invoca al método {@link
     * ArbolBinarioBusqueda#agrega}, y después balancea el árbol girándolo como
     * sea necesario. La complejidad en tiempo del método es <i>O</i>(log
     * <i>n</i>) garantizado.
     *
     * @param elemento el elemento a agregar.
     */
    @Override
    public void agregar(T elemento) {
    // Aquí va su código.
    Nodo nuevo = new NodoAVL(elemento);
    super.agregaNodo(raiz, nuevo);
    rebalancea(nodoAVL(nuevo));
        
    }

    /**
     * Elimina un elemento del árbol. El método elimina el nodo que contiene el
     * elemento, y gira el árbol como sea necesario para rebalancearlo. La
     * complejidad en tiempo del método es <i>O</i>(log <i>n</i>) garantizado.
     *
     * @param elemento el elemento a eliminar del árbol.
     */
    @Override
    public void eliminar(T elemento) {
    // Aquí va su código.
    NodoAVL aux = nodoAVL(super.buscaNodo(raiz, elemento));
    NodoAVL tmp;

    if (aux == null) {
        return;
    }

    if (aux.hayIzquierdo()) {
        tmp = aux;
        aux = nodoAVL(maximoEnSubarbolIzquierdo(aux.izquierdo));
        tmp.elemento = aux.elemento;
    }

    if (!aux.hayIzquierdo() && !aux.hayDerecho()) {
        if (raiz == aux) {
            raiz = null;
        } else if (aux.padre.izquierdo == aux){
            aux.padre.izquierdo = null;
        }else{
            aux.padre.derecho = null;
        }
    }else if (!aux.hayDerecho()) {
        if (raiz == aux) {
            raiz = raiz.izquierdo;
            raiz.padre = null;
        } else {
            aux.izquierdo.padre = aux.padre;
            if (aux.padre.izquierdo == aux) {
                aux.padre.izquierdo = aux.izquierdo;
            }else{
                aux.padre.derecho = aux.izquierdo;
            }
        }
    } else if (!aux.hayIzquierdo()) {
        if (raiz == aux) {
            raiz = raiz.derecho;
            raiz.padre = null;
        } else {
            aux.derecho.padre = aux.padre;
            if (aux.padre.izquierdo == aux) {
                aux.padre.izquierdo = aux.derecho;
            } else {
                aux.padre.derecho = aux.derecho;
            }
        }
    }
    tamanio--;
    balance(nodoAVL(aux.padre));
    }

        /**
     * Método que encuentra el elemento máximo en el subárbol izquierdo
     **/
    private Nodo maximoEnSubarbolIzquierdo(Nodo n) {
        //Aquí va tu código
            while (n.hayDerecho())
                n = n.derecho;
            return n;
        
        }

    private int getAltura(Nodo nodo) {
        // Aquí va su código.
        if (nodo == null) {
            return -1;
        }else{
            return nodoAVL(nodo).altura;
        }
    }

    /**
     * Convierte el nodo (visto como instancia de {@link
     * Nodo}) en nodo (visto como instancia de {@link
     * NodoAVL}). Método auxililar para hacer este cast en un único lugar.
     *
     * @param nodo el nodo de árbol binario que queremos como nodo AVL.
     * @return el nodo recibido visto como nodo AVL.
     */
    protected NodoAVL nodoAVL(Nodo nodo) {
        return (NodoAVL) nodo;
    }
}
