import java.util.Iterator;

/**
 * <p>
 * Clase para modelar árboles binarios de búsqueda genéricos.</p>
 *
 * <p>
 * Un árbol instancia de esta clase siempre cumple que:</p>
 * <ul>
 * <li>Cualquier elemento en el árbol es mayor o igual que todos sus
 * descendientes por la izquierda.</li>
 * <li>Cualquier elemento en el árbol es menor o igual que todos sus
 * descendientes por la derecha.</li>
 * </ul>
 *
 * @param <T>
 */
public class ArbolBinarioBusqueda<T extends Comparable<T>> extends ArbolBinario<T> {

    /* Clase privada para iteradores de árboles binarios ordenados. */
    private class Iterador implements Iterator<T> {

        /* Pila para recorrer los nodos por profundidad (DFS). */
        private Pila<Nodo> pila;

        /* Construye un iterador con el nodo recibido. */
        public Iterador() {
        //Aquí va tu código
            pila = new Pila<>();
            Nodo aux = raiz;

            while (aux != null) {
                pila.push(aux);
                aux = aux.izquierdo;
            }
        }

        /* Nos dice si hay un elemento siguiente. */
        @Override
        public boolean hasNext() {
            // Aquí va su código.
            return pila.longitud >= 1;
       	}

        /* Regresa el siguiente elemento en orden DFS in-order. */
        @Override
        public T next() {
        //Aquí va tu código
            Nodo aux = pila.pop();
            if(aux.hayIzquierdo()) pila.push(aux.izquierdo);
            if(aux.hayDerecho()) pila.push(aux.derecho);

            return aux.elemento;
        }
    }

    /**
     * Constructor que no recibe parámeteros. {@link ArbolBinario}.
     */
    public ArbolBinarioBusqueda() {

    }

    /**
     * Construye un árbol binario ordenado a partir de una colección. El árbol
     * binario ordenado tiene los mismos elementos que la colección recibida.
     *
     * @param coleccion la colección a partir de la cual creamos el árbol
     * binario ordenado.
     */
    public ArbolBinarioBusqueda(Coleccionable<T> coleccion) {
        super(coleccion);
    }

    /**
     * Método recursivo auxiliar que agrega un elemento contenido en el nodo nuevo.
     * Comienza las comparaciones desde el nodo n.
     *
     **/
    protected void agregaNodo(Nodo n, Nodo nuevo) {
    //Aquí va tu código
    if (raiz == null) {
        raiz = nuevo;
        tamanio++;
    }else{
        if( nuevo.elemento.compareTo(n.elemento) < 0) {
            if (n.izquierdo == null) {
                nuevo.padre = n;
                nuevo.izquierdo = null;
                nuevo.derecho = null;
                n.izquierdo = nuevo;
                tamanio++;
            }else{
                agregaNodo(n.izquierdo, nuevo);
            }
        }else{
            if (n.derecho == null) {
                nuevo.padre = n;
                nuevo.izquierdo = null;
                nuevo.derecho = null;
                n.derecho = nuevo;
                tamanio++;
            }else{
                agregaNodo(n.derecho, nuevo);
            }
        }
    }
}

    /**
     * Agrega un nuevo elemento al árbol. El árbol conserva su orden in-order.
     *
     * @param elemento el elemento a agregar.
     */
    @Override
    public void agregar(T elemento) {
        Nodo nuevo = nuevoNodo(elemento);
        agregaNodo(raiz, nuevo);
    }

    /**
     * Método auxiliar que elimina el nodo n. Notemos que en este punto
     * ya tenemos una referencia al nodo que queremos eliminar.
     **/
    protected Nodo eliminaNodo(Nodo n) {
    //Aquí va tu código
        if( n == raiz ){
            if( n.hayIzquierdo() ){
                Nodo aux = n;
                n = maximoEnSubarbolIzquierdo(n.izquierdo);
                aux.elemento = n.elemento;
                eliminaNodo(n);
            }
            if(!n.hayIzquierdo() && n.hayDerecho()){
                raiz = n.derecho;
            }
            if(!n.hayIzquierdo() && !n.hayDerecho()){
                vaciar();
            }
        }else{            
            if(n.hayIzquierdo() && !n.hayDerecho() ){
                n.izquierdo.padre = n.padre;
                n.padre.derecho = n.izquierdo;
            }
            if(n.hayIzquierdo() && n.hayDerecho()){
                Nodo aux = n;
                n = maximoEnSubarbolIzquierdo(n.izquierdo);
                aux.elemento = n.elemento;
                eliminaNodo(n);
            }
            if(!n.hayIzquierdo() && !n.hayDerecho()){
                if(n.padre.izquierdo == n){
                    n.padre.izquierdo = null;
                }else{
                    n.padre.derecho = null;
                }
            }
        }
        tamanio--;
        return n;

    }

    /**
     * Elimina un elemento. Si el elemento no está en el árbol, no hace nada; si
     * está varias veces, elimina el primero que encuentre (in-order). El árbol
     * conserva su orden in-order.
     *
     * @param elemento el elemento a eliminar.
     */
    @Override
    public void eliminar(T elemento) {
        Nodo n = buscaNodo(raiz, elemento);
        eliminaNodo(n);
    }

    /**
     * Método que encuentra el elemento máximo en el subárbol izquierdo
     **/
    private Nodo maximoEnSubarbolIzquierdo(Nodo n) {
    //Aquí va tu código
        while (n.hayDerecho())
            n = n.derecho;
        return n;
    
    }

    /**
     * Nos dice si un elemento está contenido en el arbol.
     *
     * @param elemento el elemento que queremos verificar si está contenido en
     * la arbol.
     * @return <code>true</code> si el elemento está contenido en el arbol,
     * <code>false</code> en otro caso.
     */
    @Override
    public boolean contiene(T elemento) {
        return buscaNodo(raiz, elemento) != null;
    }

    /**
     * Método que busca un a elemento en el árbol desde el nodo n
     **/
    protected Nodo buscaNodo(Nodo n, T elemento) {
        //Aquí va tu código
        if(n == null){
            return null;
        }
        if(elemento.compareTo(n.elemento) == 0){
            return n;
        }
        if(elemento.compareTo(n.elemento) > 0){
            return buscaNodo(n.derecho, elemento);
        }
        if(elemento.compareTo(n.elemento) < 0) {
            return buscaNodo(n.izquierdo, elemento);
        }
        return null;
    }

    /**
     * Rota el árbol a la derecha sobre el nodo recibido. Si el nodo no tiene
     * hijo izquierdo, el método no hace nada.
     *
     * @param nodo el nodo sobre el que vamos a rotar.
     */
    protected void rotacionDerecha(Nodo nodo) {
        //Aquí va tu código
            if(nodo == null || !nodo.hayIzquierdo()){
                return;
            }
            Nodo n = nodo.izquierdo;
            n.padre = nodo.padre;
            if(nodo != raiz){
                if(eshijoDerecho(nodo)){
                    n.padre.izquierdo = n;
                }else{
                    n.padre.derecho = n;
                }
            }else{
                raiz = n;
            } 
            nodo.izquierdo = n.derecho;
            if(n.hayDerecho()){
                n.derecho.padre = nodo;
            }
            nodo.padre = n;
            n.derecho = nodo;
    
        }
    
    
        /**
         * Rota el árbol a la izquierda sobre el nodo recibido. Si el nodo no tiene
         * hijo derecho, el método no hace nada.
         *
         * @param nodo el nodo sobre el que vamos a rotar.
         */
        protected void rotacionIzquierda(Nodo nodo) {
        //Aquí va tu código
            if(nodo == null || !nodo.hayDerecho()){
                return;
            }
            Nodo n = nodo.derecho;
            n.padre = nodo.padre;
            if(nodo != raiz){
                if(eshijoIzquierdo(nodo)){
                    n.padre.izquierdo = n;
                }else{
                    n.padre.derecho = n;
                }
            }else{
                raiz = n;
            }
            nodo.derecho = n.izquierdo;
            if(n.hayIzquierdo()){
                n.izquierdo.padre = n;
            }
            nodo.padre = n;
            n.izquierdo = nodo;
        }
    
        private boolean eshijoIzquierdo(Nodo n){
            if(!n.hayPadre()){
                return false;
            }
            return n.padre.izquierdo == n;
        }
    
        private boolean eshijoDerecho(Nodo n){
            if(!n.hayPadre()){
                return false;
            }
            return n.padre.derecho == n;
        }

        public void pruebaRotacionIzquierda(T elemento){
            Nodo nodo = this.buscaNodo(raiz, elemento);
            this.rotacionIzquierda(nodo);
        }
        
        public void pruebaRotacionDerecha(T elemento){
            Nodo nodo = this.buscaNodo(raiz, elemento);
            this.rotacionDerecha(nodo);
        }

    /**
     * Regresa un iterador para iterar el árbol. El árbol se itera en orden.
     *
     * @return un iterador para iterar el árbol.
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterador();
    }

}
