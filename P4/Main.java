public class Main {

    public static void main(String[] args) {

	ArbolAVL<Integer> arbol2 = new ArbolAVL<>();
	arbol2.agregar(5);
	arbol2.agregar(3);
	arbol2.agregar(1);
	arbol2.agregar(7);
	arbol2.agregar(9);
	arbol2.agregar(6);
	arbol2.agregar(15);
	arbol2.agregar(20);
	arbol2.agregar(13);
	
        ArbolAVL<Integer> arbol = new ArbolAVL<>();
	arbol.agregar(5);
	arbol.agregar(3);
	arbol.agregar(1);
	System.out.println(arbol);
	arbol.agregar(7);
	arbol.agregar(9);
	System.out.println(arbol);
	arbol.agregar(6);
	System.out.println(arbol);
	arbol.agregar(15);
	arbol.agregar(20);
	System.out.println(arbol);
	arbol.agregar(13);
	System.out.println(arbol);
	// System.out.println("---------------");
	// System.out.println(arbol.equals(arbol2)); // true
	// arbol.eliminar(5);
	// System.out.println(arbol.equals(arbol2)); // false
	// arbol2.eliminar(5);
	// System.out.println(arbol.equals(arbol2)); // true
	// System.out.println("---------------");
	
	System.out.println("------Eliminar------");

	System.out.println(arbol);
	arbol.eliminar(17);
	System.out.println(arbol);
	arbol.eliminar(1);
	System.out.println(arbol);
	arbol.eliminar(6);
	System.out.println(arbol);
	arbol.eliminar(15);
	System.out.println(arbol);
	arbol.eliminar(9);
	System.out.println(arbol);

	System.out.println("---------------");
    }
}
