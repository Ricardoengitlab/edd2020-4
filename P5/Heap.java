import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Clase abstracta para modelar montículos. Las clases concretas pueden ser un montículo mínimo
 * o máximo.
 */
public abstract class Heap<T extends Comparable<T>> implements Coleccionable<T> {

    /**
     * Clase interna para modelar el iterador
     */
    private class Iterador implements Iterator<T> {

        private int siguiente;

        @Override
        public boolean hasNext() {
        //Aquí va tu código
            return siguiente < arreglo.length && arreglo[siguiente] != null;            
        }

        @Override
        public T next() {
        //Aquí va tu código
        if(!hasNext()) throw new NoSuchElementException();
        return arreglo[siguiente++];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

    /**
     * Arreglo donde se almacenarán los elementos del montículo.
     **/
    private T[] arreglo;
    /**
     * Cantidad de elementos almacenados en el montículo.
     **/
    private int tamanio;
    
    
    /* Truco para crear arreglos genéricos. Es necesario hacerlo así por cómo
       Java implementa sus genéricos; de otra forma obtenemos advertencias del
       compilador. */
    @SuppressWarnings("unchecked") private T[] creaArregloGenerico(int n) {
        return (T[])(new Comparable[n]);
    }

    /**
     * Constructor que no recibe parámetros, crea un arreglo de un tamaño arbitrario.
     * Se recomienda que sea de un tamaño que sea una potencia de 2.
     **/
    public Heap() {
    //Aquí va tu código
        tamanio = 0;
        arreglo = creaArregloGenerico(1024);
    }

    /**
     * Constructor que recibe una estructura iterable como parámetro.
     * Agrega todos los elementos en el orden en que se recorre la estructura dada.
     **/
    public Heap(Iterable<T> it) {
    //Aquí va tu código
        tamanio = 0;
        int c = 0;
        for( T elem: it){
            c++;
        }
        arreglo = creaArregloGenerico(c);
        for (T elem : it){
            arreglo[tamanio++] = elem;
        }
        for (int j = (tamanio - 1); j >= 0; --j) {
            rebalanceaHaciaAbajo(j);
        }
    }
    
    /**
     * Método abstracto que se va a usar para comparar dos elementos del heap.
     * Se deja la implementación a las clases concretas, pues dependiendo de éstas el orden es
     * uno o el inverso, según sea el caso.
     * @param elemento1
     * @param elemento2
     * @return true si elemento1 tiene mayor prioridad que elemento2, false en otro caso
     */
    abstract protected boolean comparador(T elemento1,T elemento2);


    /**
     * Método que nos da la posición del padre del índice dado
     **/
    private int padre(int indiceElemento) {
    //Aquí va tu código
        return (indiceElemento - 1) / 2;
    }

    /**
     * Método que nos da la posición del hijo izquierdo del índice dado
     **/
    private int izquierdo(int indiceElemento) {
    //Aquí va tu código
        return (indiceElemento * 2 ) + 1;
    }

    /**
     * Método que nos da la posición del hijo derecho del índice dado
     **/
    private int derecho(int indiceElemento) {
    //Aquí va tu código
        return (indiceElemento * 2 ) + 2;
    }

    @Override
    public void agregar(T elemento) {
    //Aquí va tu código
        if (tamanio >=arreglo.length) {
            T[] aux = creaArregloGenerico(arreglo.length * 2);
            for (int i = 0; i < aux.length; i++) {
                aux[i] = arreglo[i];
            }
            arreglo = aux;
        }
        arreglo[tamanio] = elemento;
        
        rebalanceaHaciaArriba(tamanio++);
        
    }
    
    /**
     * Metodo para eliminar el elemento que se encuentra en el tope del heap.
     * El método devuelve el valor eliminado.
     */
    public T eliminarTope() {
    //Aquí va tu código
        if(esVacia())
            throw new IllegalStateException();
        rebalanceaHaciaArriba(0);
        T aux = arreglo[0];
        intercambia(0,--tamanio);
        return aux;
    }

    /**
     * Método para intercambiar dos elementos en los índices i y j.
     * Antes de usarse debemos asegurarnos de que los índices sean válidos.
     **/
    private void intercambia(int i, int j) {
    //Aquí va tu código
        T x = arreglo[i];
        T y = arreglo[j];

        arreglo[i] = y;
        arreglo[j] = x;
    }

    /**
     * Metodo que se encarga de hacer el rebalanceo cuando agregamos un elemento.
     * @param indiceElemento
     */
    private void rebalanceaHaciaArriba(int indiceElemento) {
    //Aquí va tu código
        int padre = padre(indiceElemento);
        int menor = indiceElemento;

        if (padre >= 0 && comparador(arreglo[indiceElemento], arreglo[padre]))
        menor = padre;

        if (menor != indiceElemento) {
            T aux = arreglo[indiceElemento];

            arreglo[indiceElemento] = arreglo[padre];
            //arreglo[indiceElemento].setIndice(indiceElemento);

            arreglo[padre] = aux;
            //arreglo[padre].setIndice(padre);

            rebalanceaHaciaArriba(menor);
        }
    
    }

    /**
     * Metodo que se encarga de hacer el rebalanceo cuando eliminamos un elemento.
     * @param indiceElemento
     */
    private void rebalanceaHaciaAbajo(int indiceElemento) {
    //Aquí va tu código
        if (indiceElemento < 0 || indiceElemento >= getTamanio() )
        return;
        T aux;    
        int menor = indiceElemento;
        int izq = izquierdo(indiceElemento);
        int der = derecho(indiceElemento);

        if (izq < getTamanio() && comparador(arreglo[menor], arreglo[izq]))
            menor = izq;
        if (der < getTamanio()  && comparador(arreglo[menor], arreglo[der]))
            menor = der;
            
        if (menor != indiceElemento) {
            aux = arreglo[indiceElemento];
			
            //arreglo[i].setIndice(menor);
            arreglo[indiceElemento] = arreglo[menor];
            
            //arreglo[menor].setIndice(i);
            arreglo[menor] = aux;
            
            rebalanceaHaciaAbajo(menor);
        }
    }

    /**
     * Método que nos dice cuál es el índice del elemento que tenemos que intercambiar con el padre.
     * Se utiliza en rebalanceaHaciaAbajo.
     * Si no hay que hacer intercambios porque ya no hay hijos, debe devolver -1.
     **/
    private int indiceItercambiable( int i, int j) {
    //Aquí va tu código
        if (j >= tamanio)
        return i;
        else if (arreglo[i].compareTo(arreglo[j]) < 0)
            return i;
        else
            return j;
    }

    /**
     * Metodo para obtener el elemento que se encuentra en el tope del heap
     * @return
     * @throws NoSuchElementException
     */
    public T obtenerPrioritario() throws NoSuchElementException{
    //Aquí va tu código
        if (esVacia()) {
            throw new NoSuchElementException();
        }else{
            return arreglo[0];
        }
    }

    @Override
    public void eliminar(T elemento) throws NoSuchElementException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean contiene(T elemento) {
    //Aquí va tu código
        for(T elem : arreglo){
            if(elem != null && elem.equals(elemento)){
                System.out.println(elem);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean esVacia() {
    //Aquí va tu código
        return arreglo == null || arreglo[0] == null;
    }

    @Override
    public int getTamanio() {
    //Aquí va tu código
        return tamanio;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterador();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        Heap<T> heap = (Heap<T>) o;
    //Aquí va tu código
        if(heap.getTamanio() != getTamanio()){
            return false;
        }
        for (int i = 0; i < arreglo.length; i++) {
            if(arreglo[i] != heap.arreglo[i]){
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        if (esVacia()) {
            return "[]";
        }
        String s = "[";
        for (int i = 0; i < tamanio - 1; i++) {
            s += arreglo[i] + ", ";
        }
        s += arreglo[tamanio-1] + "]";

        return s;
    }

}
