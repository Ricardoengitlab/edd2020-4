### Prácticas del curso de estructuras de datos

#### Práctica 1
- Listas

#### Práctica 2
- Pila y Cola

#### Práctica 3
- Árbol binario de búsqueda

#### Práctica 4
- Árbol AVL

#### Práctica 5
- Heap
